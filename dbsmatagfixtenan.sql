-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2020 at 03:25 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbsmatagfixtenan`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggotaekskul`
--

CREATE TABLE `anggotaekskul` (
  `id` int(11) NOT NULL,
  `jabatan` int(11) DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `extrakurikuler_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ekstrakurikuler`
--

CREATE TABLE `ekstrakurikuler` (
  `idekstrakurikuler` int(11) NOT NULL,
  `namaekskul` varchar(255) DEFAULT NULL,
  `deskripsiekskul` text,
  `tahunajaran` varchar(11) DEFAULT NULL,
  `logo_ekskul` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ekstrakurikuler`
--

INSERT INTO `ekstrakurikuler` (`idekstrakurikuler`, `namaekskul`, `deskripsiekskul`, `tahunajaran`, `logo_ekskul`) VALUES
(2, 'renang', 'renang adalah', '2020/2021', 'logo_ekskul/978680578.jpg.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `fileproposal`
--

CREATE TABLE `fileproposal` (
  `id` int(11) NOT NULL,
  `fileproposal` varchar(1000) DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `ekstrakurikuler_id` int(11) NOT NULL,
  `tahunajaran_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `isi` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_proposal`
--

CREATE TABLE `jenis_proposal` (
  `id` int(11) NOT NULL,
  `jenis_proposal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `konten`
--

CREATE TABLE `konten` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `fotoposting` varchar(1000) DEFAULT NULL,
  `tglpostingan` date DEFAULT NULL,
  `deskripsi_singkat` text,
  `isi` longtext,
  `video` varchar(1000) DEFAULT NULL,
  `user_id` bigint(11) UNSIGNED DEFAULT NULL,
  `ekstrakurikuler_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proposal`
--

CREATE TABLE `proposal` (
  `idproposal` int(11) NOT NULL,
  `judulproposal` varchar(255) DEFAULT NULL,
  `jenisproposal` varchar(255) DEFAULT NULL,
  `tahunajaranproprosal` varchar(255) DEFAULT NULL,
  `ekstrakurikuler_idekstrakurikuler` int(11) NOT NULL,
  `jenis_proposal_id` int(11) NOT NULL,
  `tahunajaran` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `proposal_has_user`
--

CREATE TABLE `proposal_has_user` (
  `proposal_idproposal` int(11) NOT NULL,
  `users_idusers` bigint(20) UNSIGNED NOT NULL,
  `is_prposal_admin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id` int(11) NOT NULL,
  `tahun_ajaran` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kelas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peran` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `kelas`, `peran`, `status`) VALUES
(1, 'guru_yusuf', 'guru1@gmail.com', NULL, '$2y$10$K3KH3rdnElPT8WduFOy9HedH5Y9IsWbA3uk15Cboi51ZYe58pw2ES', NULL, NULL, NULL, '-', 1, 1),
(2, 'ardi', 'ardi@gmail.com', NULL, '$2y$10$vllfzPEig6NUXBaTq1dqUeFLOBA.HBCvkyBRNPBpvKjZanWMc0C5q', NULL, NULL, NULL, 'X-P1', 2, 1),
(3, 'enas', 'enas@gmail.com', NULL, '$2y$10$mPANBfCWCC26mmXbeufYC.5EMVlnDG/dUU1jFlHufolvmTHr8aoJ.', NULL, NULL, NULL, 'X-P1', 2, 1),
(4, 'gagah', 'gagah@gmail.com', NULL, '$2y$10$Zdthh6U.8Jg34Yq2c8RTXOFbbV6ae8ot6obKeYiI7bMo/5SHG1jGy', NULL, NULL, NULL, 'X-P1', 2, 1),
(5, 'indra', 'indra@gmail.com', NULL, '$2y$10$svwI1alha6s2kQm6WWMqAuhcgae57zua4wDDAXcwzCv1sj7Z7/MJa', NULL, NULL, NULL, 'XI-P1', 2, 1),
(6, 'iman', 'iman@gmail.com', NULL, '$2y$10$c.1F5Pvn69.WaM/.6ZQzS.YpYGJjEtueUQ2zSmAjQ5Tw6fGiE3sdS', NULL, NULL, NULL, 'XI-S1', 2, 1),
(7, 'valih', 'valih@gmail.com', NULL, '$2y$10$V74i2YAYkVDIbU/VgL.KH.l27yyI02qP.UwG8J1bpUICHTs4c5hLm', NULL, NULL, NULL, 'XI-P3', 2, 1),
(8, 'maydina', 'maydina@gmail.com', NULL, '$2y$10$8AeIToTliDbdNES0VZWut.AJouGdOT3awc./yMP5P6FiProt0Kj4S', NULL, NULL, NULL, 'XI-P4', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_halaman`
--

CREATE TABLE `user_has_halaman` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `halaman_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_kelas`
--

CREATE TABLE `user_has_kelas` (
  `id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggotaekskul`
--
ALTER TABLE `anggotaekskul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `extrakurikuler_id` (`extrakurikuler_id`);

--
-- Indexes for table `ekstrakurikuler`
--
ALTER TABLE `ekstrakurikuler`
  ADD PRIMARY KEY (`idekstrakurikuler`);

--
-- Indexes for table `fileproposal`
--
ALTER TABLE `fileproposal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_fileproposal_ekstrakurikuler1_idx` (`ekstrakurikuler_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `tahunajaran_id` (`tahunajaran_id`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposal_id` (`proposal_id`);

--
-- Indexes for table `jenis_proposal`
--
ALTER TABLE `jenis_proposal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konten`
--
ALTER TABLE `konten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `ekstrakurikuler_id` (`ekstrakurikuler_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposal`
--
ALTER TABLE `proposal`
  ADD PRIMARY KEY (`idproposal`),
  ADD KEY `fk_proposal_ekstrakurikuler1_idx` (`ekstrakurikuler_idekstrakurikuler`),
  ADD KEY `jenis_proposal_id` (`jenis_proposal_id`);

--
-- Indexes for table `proposal_has_user`
--
ALTER TABLE `proposal_has_user`
  ADD PRIMARY KEY (`proposal_idproposal`,`users_idusers`),
  ADD KEY `fk_proposal_has_user_proposal1_idx` (`proposal_idproposal`),
  ADD KEY `users_idusers` (`users_idusers`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_has_halaman`
--
ALTER TABLE `user_has_halaman`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `halaman_id` (`halaman_id`);

--
-- Indexes for table `user_has_kelas`
--
ALTER TABLE `user_has_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggotaekskul`
--
ALTER TABLE `anggotaekskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekstrakurikuler`
--
ALTER TABLE `ekstrakurikuler`
  MODIFY `idekstrakurikuler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fileproposal`
--
ALTER TABLE `fileproposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis_proposal`
--
ALTER TABLE `jenis_proposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `konten`
--
ALTER TABLE `konten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proposal`
--
ALTER TABLE `proposal`
  MODIFY `idproposal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_has_kelas`
--
ALTER TABLE `user_has_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggotaekskul`
--
ALTER TABLE `anggotaekskul`
  ADD CONSTRAINT `anggotaekskul_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `anggotaekskul_ibfk_2` FOREIGN KEY (`extrakurikuler_id`) REFERENCES `ekstrakurikuler` (`idekstrakurikuler`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fileproposal`
--
ALTER TABLE `fileproposal`
  ADD CONSTRAINT `fileproposal_ibfk_1` FOREIGN KEY (`ekstrakurikuler_id`) REFERENCES `ekstrakurikuler` (`idekstrakurikuler`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fileproposal_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fileproposal_ibfk_3` FOREIGN KEY (`tahunajaran_id`) REFERENCES `tahun_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `halaman`
--
ALTER TABLE `halaman`
  ADD CONSTRAINT `halaman_ibfk_1` FOREIGN KEY (`proposal_id`) REFERENCES `proposal` (`idproposal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `konten`
--
ALTER TABLE `konten`
  ADD CONSTRAINT `konten_ibfk_1` FOREIGN KEY (`ekstrakurikuler_id`) REFERENCES `ekstrakurikuler` (`idekstrakurikuler`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `konten_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `proposal`
--
ALTER TABLE `proposal`
  ADD CONSTRAINT `proposal_ibfk_1` FOREIGN KEY (`ekstrakurikuler_idekstrakurikuler`) REFERENCES `ekstrakurikuler` (`idekstrakurikuler`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proposal_ibfk_2` FOREIGN KEY (`jenis_proposal_id`) REFERENCES `jenis_proposal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `proposal_has_user`
--
ALTER TABLE `proposal_has_user`
  ADD CONSTRAINT `proposal_has_user_ibfk_1` FOREIGN KEY (`proposal_idproposal`) REFERENCES `proposal` (`idproposal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proposal_has_user_ibfk_2` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_has_halaman`
--
ALTER TABLE `user_has_halaman`
  ADD CONSTRAINT `user_has_halaman_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_has_halaman_ibfk_2` FOREIGN KEY (`halaman_id`) REFERENCES `halaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_has_kelas`
--
ALTER TABLE `user_has_kelas`
  ADD CONSTRAINT `user_has_kelas_ibfk_1` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_has_kelas_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
