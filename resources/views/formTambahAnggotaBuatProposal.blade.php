@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Buat Proposal Kegiatan</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">
                    <input type="hidden" name="idkelas" class="idkelas" value="">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Judul Proposal :</label>
                        <div class="col-md-5">
                            <input type="" id="nama_kegiatan" name="nama_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Jenis Proposal :</label>
                        <div class="col-md-5">
                            <input type="" id="tglKegiatan" name="tanggal_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Foto Logo Sekolah :</label>
                        <div class="col-md-5">
                            <input type="file" id="namafile" name="nama_file">
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Footer Proposal :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Status :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Tahun Ajaran :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Ekstrakurikuler :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                    <a href="#" onclick="Selanjutnya()" id="tambah" class="btn btn-warning btn-save btn-right">Selanjutnya</a>         
                    </div>
                    <br>
                    <br>
                </div>
            </form>
            <hr>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>Nama Anggota</th>
                        <th>Email</th>
                        <th>Kelas</th>
                        <th>Peran</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="" class="badge badge-success">Hapus</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a href="/formperbabproposal" onclick="Selesai()" class="btn btn-warning btn-save btn-block">Selesai</a>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection