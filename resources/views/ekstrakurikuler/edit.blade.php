@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Edit Ekstrakurikuler</h1>
            <form class="form-horizontal" action="{{url('ekstrakurikuler/update/'.$ekstrakurikuler->idekstrakurikuler)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field()}} {{method_field('POST')}}
                    <div class="form-group">
                        <label class="col-md-4 control-label">Pilih Foto :</label>
                        <div class="col-md-5">
                            <input type="file" id="logo_ekskul" name="logo_ekskul">
                            @error('logo_ekskul')
                                <span class="text-danger" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Ekskul :</label>
                        <div class="col-md-5">
                            <input type="text" id="nama" name="nama_ekskul" value="{{ $ekstrakurikuler->namaekskul }}">
                            @error('nama_ekskul')
                                <span class="text-danger" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tahun Ajaran :</label>
                        <div class="col-md-5">
                            <input type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control" value="{{$ekstrakurikuler->tahunajaran}}" >
                            @error('tahun_ajaran')
                                <span class="text-danger" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Deskripsi Kegiatan :</label>
                        <div class="col-md-5">
                            <textarea id="deskripsi_kegiatan" name="deskripsi_kegiatan" class="form-control"></textarea>
                            @error('deskripsi_kegiatan')
                                <span class="text-danger" style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                    <button class="btn btn-success">Update</button>         
                    </div>
                    <br>
                    <br>
            </form>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
<!-- modal -->
@endsection
@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('deskripsi_kegiatan');
        CKEDITOR.instances['deskripsi_kegiatan'].setData("{{$ekstrakurikuler->deskripsiekskul}}");
    </script>
@endsection