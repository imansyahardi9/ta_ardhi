@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <div class="form-group">
                <div class="col-md-3">
                    <select class="form-control" onchange="gantiekskul()" id="ekskul">
                        @foreach ($ekstrakurikulers as $item)
                            <option @if($item->idekstrakurikuler == $id) selected @endif value="{{ $item->idekstrakurikuler }}">{{ $item->namaekskul }}</option>
                        @endforeach
                    </select>
                </div>
                
                    <a href="{{ url('ekstrakurikuler/edit/'.$ekstrakurikuler->idekstrakurikuler) }}" class="btn btn-warning btn-save btn-right">Ubah Informasi Ekskul</a >
                
                
                    <a href="{{ url('ekstrakurikuler/create') }}" class="btn btn-warning btn-save btn-right">Tambah Ekskul</a >

                    <a href="{{ url('ekstrakurikuler/delete/'.$ekstrakurikuler->idekstrakurikuler) }}" class="btn btn-warning btn-save btn-right">Hapus Ekskul</a >
                
                
                    <a href="#" class="btn btn-warning btn-save btn-right">Tambah Anggota Ekskul</a >

                        
                
            </div>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <hr>
            <center>
                <p>STRUKTUR EKSTRAKURIKULER</p>
                <p>{{ strtoupper($ekstrakurikuler->namaekskul) }}</p>
                <p>SMA 17 AGUSTUS 1945 SURABAYA</p>
                <p>TAHUN PELAJARAN {{ $ekstrakurikuler->tahunajaran }}</p>
            </center>
            <hr>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <img src="{{asset($ekstrakurikuler->logo_ekskul)}}" style="width:100%;height:300px">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-12">
            <hr>
            {!! $ekstrakurikuler->deskripsiekskul !!}
        </div>
    </div>
</div>
<!-- modal -->
@endsection
@section('script')
    <script type="text/javascript">
        // $(document).ready( function () {
        //     $('#table_id').DataTable();
        // } );
        function gantiekskul(){
            location.href = "{{ url('ekstrakurikuler/index') }}/"+$('#ekskul').val();
        }
    </script>
@endsection