@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>  
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tpl-blog-holder" apply-isotope="">
                    <h1>Tambah Anggota Proposal: </h1>
                    <form class="form-horizontal" action="{{url('crud_tambah_anggota')}}" data-toggle="validator" method="post" id="form-learning-goal">
                        {{ csrf_field()}} {{method_field('POST')}}
                            <input type="hidden" name="proposal_idproposal" value="{{$proposal_id}}">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Anggota :</label>
                                <div class="col-md-5">
                                    <select class="select2 form-control"  multiple="multiple" name="users_idusers[]"  required>
                                        @foreach($users as $user)
                                            <option value="{{$user['id']}}">{{$user['nama']}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary">submit</button>         
                            </div>
                            <br>
                            <br>
                    </form>
                </div>
            </div>                          
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function() {
	    $('.select2').select2();
	    $.ajax({
	    	url: "{{url('get_anggota_proposal')}}/{{$proposal_id}}",
	    	type: "GET",
	    	dataType: "json",
	    	success: function(result){
	    		$('.select2').val(result).change();
			}
		});
	});

</script>
@endsection