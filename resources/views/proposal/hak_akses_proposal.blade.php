@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>  
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tpl-blog-holder" apply-isotope="">
                    <h1>Hak Akses Pembuatan Proposal</h1>
                    <form class="form-horizontal" action="{{url('insert_tambah_anggota_halaman_proposal')}}" data-toggle="validator" method="post" id="form-learning-goal">
                        {{ csrf_field()}} {{method_field('POST')}}
                            <input type="hidden" name="proposal_id" value="{{$proposal_id}}">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Siswa:</label>
                                <div class="col-md-5">
                                    <select class="select2 form-control"  multiple="multiple" name="user_id[]"  required>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->nama}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Keterangan Halaman</label>
                                <div class="col-md-5">
                                    <input type="text" name="keterangan">
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary">submit</button>         
                            </div>
                            <br>
                            <br>
                    </form>
                </div>
            </div>                          
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tpl-blog-holder" apply-isotope="">
                    <table id="table_id">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Keterangan</th>
                                <th>Hak Akses</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($halaman as $key => $halaman)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$halaman['keterangan']}}</td>
                                    <td>{!!$halaman['anggota']!!}</td>
                                    <td>
                                        <div class="dropdown">
                                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">edit
                                          </button>
                                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">hapus
                                          </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
	// $(document).ready(function() {
	    $('.select2').select2();
        $('#table_id').DataTable();
	//     $.ajax({
	//     	url: "{{url('get_anggota_proposal')}}/{{$proposal_id}}",
	//     	type: "GET",
	//     	dataType: "json",
	//     	success: function(result){
	//     		alert(result);
	//     		console.log(result);
	//     		$('.select2').val(result).change();
	// 		}
	// 	});
	// });
</script>
@endsection