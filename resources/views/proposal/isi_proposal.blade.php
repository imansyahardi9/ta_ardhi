@extends('layouts.app')

@section('content')
<!-- --------------------------------------------------ISI WEBSITE-------------------------------------------------- --><br>
<br>
<br>
<br>  
<div class="container">
	<div class="row">
		<br>
		<h1>Form Proposal Kegiatan</h1>
		<a href="" style="float: right;" class="btn btn-primary">View All</a>
	</div>
    <div class="row">
		<div class="card">
            <div class="card-body" id="container-halaman">
            	{!! $halaman !!}
        	</div>
        </div>
        <hr>
        <a class="btn btn-primary" id="tambahHalaman" style="display: none;" onclick="tambahHalaman()">tambah halaman</a>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready( function () {
    	@foreach ($halamans as $value)
    		CKEDITOR.replace('halaman-'+{{$value->id}});
    		$.ajax({
		    	url: "{{url('get_isi_halaman/'.$value->id)}}",
		    	type: "GET",
		    	success: function(result){
		    		CKEDITOR.instances['halaman-'+{{$value->id}}].setData(result);		
				}
			});
    		
    		CKEDITOR.instances['halaman-'+{{$value->id}}].on('change', function() {
    			var isi_proposal = CKEDITOR.instances['halaman-'+{{$value->id}}].getData();
    			$.ajax({
				  type: "POST",
				  url: "{{url('update_isi_proposal/'.$value->id)}}",
				  data: {isi_proposal:isi_proposal, _token: "{{ csrf_token() }}"},
			      success: function (response) {

			           // You will get response from your PHP page (what you echo or print)
			      },
				});
    		});
    	@endforeach
        
    });
    function set_header(param1,param2){
    	var result = $('#combobox-'+param2).val();
    	CKEDITOR.instances[param1].insertHtml('<h3 style><b>'+result+'<b></h3>');
    }

    // $('.halaman').on('input',function(){
    // 	cosole.log($(this).text());
    // 	cosole.log($(this).attr('id'));
    // });
    
    function buttonHeder(id,idhalaman){
	    // CKEDITOR.instances[ckeditor_id].insertText(str);
	    if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }else if(id){

	    }
	}
</script>
@endsection