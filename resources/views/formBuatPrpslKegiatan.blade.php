@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Buat Proposal Kegiatan</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">
                    <input type="hidden" name="idkelas" class="idkelas" value="">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Judul Proposal :</label>
                        <div class="col-md-5">
                            <input type="" id="nama_kegiatan" name="nama_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Jenis Proposal :</label>
                        <div class="col-md-5">
                            <input type="" id="tglKegiatan" name="tanggal_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Foto Logo Sekolah :</label>
                        <div class="col-md-5">
                            <input type="file" id="namafile" name="nama_file">
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Footer Proposal :</label>
                        <div class="col-md-5">
                            <textarea id="footerProposal" class="form-control" autofocus required></textarea>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Status :</label>
                        <div class="col-md-5">
                            <select name="jabatan" class="form-control">
                                <option value="01">Proses</option>
                                <option value="02">Pengerjaan</option>
                                <option value="03">Selesai</option>
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Tahun Ajaran :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required value="{{date('Y/Y')}}">
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Ekstrakurikuler :</label>
                        <div class="col-md-5">
                            <input type="" id="lokasi_kegiatan" name="lokasi_kegiatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                    <a href="/tambahanggotabuatproposal" onclick="Selanjutnya()" id="tambah" class="btn btn-warning btn-save btn-right">Selanjutnya</a>         
                    </div>
                    <br>
                    <br>
                </div>
            </form>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection