@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>  
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tpl-blog-holder" apply-isotope="">
                    <h1>Form Unggah File Proposal (PDF)</h1>
                    <form class="form-horizontal" action="insertproposal" data-toggle="validator" method="post" id="form-learning-goal">
                        {{ csrf_field()}} {{method_field('POST')}}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Judul Proposal :</label>
                                <div class="col-md-5">
                                    <input type="text" id="judulproposal" name="judulproposal" class="form-control" autofocus required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Jenis Proposal :</label>
                                <div class="col-md-5">
                                    <input type="text" id="jenisproposal" name="jenisproposal" class="form-control" autofocus required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Tahun Ajaran :</label>
                                <div class="col-md-5">
                                    <input type="text" id="tahunajaranproprosal" name="tahunajaranproprosal" class="form-control" autofocus required>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Ekstrakurikuler :</label>
                                <div class="col-md-5">
                                    <select class="form-control" name="ekstrakurikuler_idekstrakurikuler" autofocus required>
                                        @foreach($extrakulikuler as $extrakulikuler)
                                            <option value="{{$extrakulikuler->idekstrakurikuler}}">{{$extrakulikuler->namaekskul}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>

                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-warning btn-save btn-right">BUAT</button>         
                            </div>
                            <br>
                            <br>
                    </form>
                </div>
            </div>                          
        </div>
        <div class="row">
            <table id="table_id">
                <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Jenis</th>
                        <th>Extrakulikuler</th>
                        <th>Tahun ajaran</th>
                        <th>Anggota</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>{{$proposal['judulproposal']}}</td>
                            <td>{{$proposal['jenisproposal']}}</td>
                            <td>{{$proposal['namaekskul']}}</td>
                            <td>{{$proposal['tahunajaranproprosal']}}</td>
                            <td>{!!$proposal['anggota']!!}</td>
                            <td>
                                <div class="dropdown">
                                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    aksi
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="{{url('tambahanggotaproposal/'.$proposal['idproposal'])}}" class="btn btn-sm btn-default col-md-12 dropdown-item">Tambah Anggota</a><br>
                                    <a href="{{url('hak_akses_proposal/'.$proposal['idproposal'])}}" class="btn btn-sm btn-default col-md-12 dropdown-item">Hak Akses Proposal</a><br>
                                    <a href="{{url('siswa/isi_proposal/'.$proposal['idproposal'])}}" class="btn btn-sm btn-default col-md-12 dropdown-item">Ubah Isi Proposal</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">Ubah Informasi Proposal</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">Hapus Proposal</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">Unduh Proposal</a><br>
                                  </div>
                                </div>
                            </td>
                        </tr>
                </tbody>
            </table>   
        </div>
    </div>
<!-- modal -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection