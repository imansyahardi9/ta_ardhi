@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Ubah Data Diri Anggota Sekolah</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">
                    <input type="hidden" name="" class="" >
                   
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama :</label>
                        <div class="col-md-5">
                            <input type="" id="nama" name="nama" class="form-control" value="" autofocus>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Email :</label>
                        <div class="col-md-5">
                            <input type="" id="email" name="email" class="form-control" value="" autofocus>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Kelas :</label>
                        <div class="col-md-5">
                            <input type="" id="kelas" name="kelas" class="form-control" value="" autofocus>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Peran :</label>
                        <div class="col-md-5">
                            <select id="peran" name="peran" class="form-control">
                                 <option value="02">Siswa</option>
                                 <option value="01">Admin</option>
                             </select>
                            <span class="help-block with-errors" ></span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Status :</label>
                        <div class="col-md-5">
                            <select id="status" name="status" class="form-control">
                                 <option value="01">Aktif</option>
                                 <option value="02">Tidak aktif</option>
                             </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                    <a href="/dataanggotasekolah" onclick="simpan()" id="simpan" class="btn btn-warning btn-save btn-right">SIMPAN</a>         
                    </div>
                    <br>
                    <br>
                </div>
            </form>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection