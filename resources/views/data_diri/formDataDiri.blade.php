@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Data Diri</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">
                    <input type="hidden" name="" class="" >
                   
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama :</label>
                        <div class="col-md-5">
                            <input type="" id="nama" name="nama" class="form-control" value="{{$datadiri->nama}}" autofocus disabled>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Email :</label>
                        <div class="col-md-5">
                            <input type="" id="email" name="email" class="form-control" value="{{$datadiri->email}}" autofocus disabled>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Kelas :</label>
                        <div class="col-md-5">
                            <input type="" id="kelas" name="kelas" class="form-control" value="{{$datadiri->kelas}}" autofocus disabled>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Peran :</label>
                        <div class="col-md-5">
                            <select id="peran" name="peran" class="form-control" disabled>
                                 <option value="02" @if($datadiri->peran == 2) selected @endif >Siswa</option>
                                 <option value="01" @if($datadiri->peran == 1) selected @endif >Admin</option>
                             </select>
                            <span class="help-block with-errors" ></span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Status :</label>
                        <div class="col-md-5">
                            <select id="status" name="status" class="form-control" disabled>
                                 <option value="01" @if($datadiri->peran == 1) selected @endif >Aktif</option>
                                 <option value="02"  @if($datadiri->peran == 2) selected @endif >Tidak aktif</option>
                             </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection