@extends('layouts.app')


@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
            <table id="table_id">
                <thead>
                    <tr>
                        <th>Nama Kegiatan</th>
                        <th>Tanggal Kegiatan</th>
                        <th>Lokasi Kegiatan</th>
                        <th>Deskripsi Kegiatan</th>
                        <th>Ketua Kegiatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <div class="dropdown">
                                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    aksi
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">edit proposal</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item"  >Tambah Anggota</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">Setting Proposal</a><br>
                                    <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">edit informasi</a><br>
                                  </div>
                                </div>
                            </td>
                        </tr>

                </tbody>
            </table>   
    </div>
</div>
<!-- modal -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection