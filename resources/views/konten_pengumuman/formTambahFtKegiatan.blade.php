@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Form Tambah Konten Kegiatan</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">
                    <input type="hidden" name="idkelas" class="idkelas" value="">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Acara Kegiatan :</label>
                        <div class="col-md-5">
                            <input type="" id="nama_acrpostingan" name="nama_acrpostingan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Foto Kegiatam:</label>
                        <div class="col-md-5">
                            <input type="file" id="namafile" name="nama_file">
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Tanggal Kegiatan :</label>
                        <div class="col-md-5">
                            <input type="date" id="tglkegiatanpostingan" name="tanggal_kegiatanpostingan" class="form-control" autofocus required value="{{date('Y-m-d')}}" >
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Deskripsi Kegiatan :</label>
                        <div class="col-md-5">
                            <textarea id="deskripsi_postingan" class="form-control"></textarea>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                    <a href="#" onclick="Tambah()" id="tambah" class="btn btn-warning btn-save btn-right">TAMBAH</a>         
                    </div>
                    <br>
                    <br>
                </div>
                <div class="modal-footer"></div>
            </form>
            <hr>

            
            <div class="row">
                <table id="table_id">
                    <thead>
                        <tr>
                            <th>Nama Kegiatan</th>
                            <th>Foto Kegiatan</th>
                            <th>Tanggal Kegiatan</th>
                            <th>Deskripsi Kegiatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <div class="dropdown">
                                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        aksi
                                      </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">edit proposal</a><br>
                                        <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item"  >Tambah Anggota</a><br>
                                        <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">Setting Proposal</a><br>
                                        <a href="" class="btn btn-sm btn-default col-md-12 dropdown-item">edit informasi</a><br>
                                      </div>
                                    </div>
                                </td>
                            </tr>

                    </tbody>
                </table>   
            </div>
            <!-- <a href="#" onclick="Unggahft()" class="btn btn-warning btn-save btn-block">UNGGAH FOTO</a> -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
<!-- modal -->
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection