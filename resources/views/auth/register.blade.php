
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BUAT AKUN BARU</title>

        <!-- Bootstrap Core CSS -->
        <link href="{{asset('startmin-master/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="{{asset('startmin-master/css/metisMenu.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{asset('startmin-master/css/startmin.css')}}" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="{{asset('startmin-master/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Buat Akun Baru</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ url('/buatakun') }}">
                                @csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label>Peran</label>
                                         <select id="peran" name="peran" class="form-control">
                                             <option value="2">Siswa</option>
                                             <option value="1">Admin</option>
                                         </select>
                                    </div>

                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input id="email" placeholder="E-mail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label>Kata sandi</label>
                                        <input id="password" placeholder="Kata Sandi" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input id="nama" placeholder="Nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label>Ekstrakurikuler</label>
                                         <select id="ekstrakurikuler" name="ekstrakurikuler" placeholder="ekstrakurikuler" class="form-control">
                                            <option value="Basket">Baket</option>
                                            <option value="Tari">Tari</option>
                                            <option value="Paskibra">Paskibra</option>
                                            <option value="PMI">PMI</option>
                                            <option value="Adiwiyata">Adiwiyata</option>
                                            <option value="Renang">Renang</option>
                                            <option value="Volly">Volly</option>
                                            <option value="Karate">Karate</option>
                                            <option value="Padus">Padus</option>
                                         </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Jabatan</label>
                                         <select id="jabatan" name="jabatan" placeholder="jabatan" class="form-control">
                                            <option value="Pembina">Pembina</option>
                                            <option value="Ketua">Ketua</option>
                                            <option value="Sekretaris">Sekretaris</option>
                                            <option value="Bendahara">Bendahara</option>
                                            <option value="Koor. Latihan">Koor. Latihan</option>
                                            <option value="Koor. Latihan">Koor. Latihan</option>
                                            <option value="Anggkota">Anggkota</option>
                                         </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Kelas</label>
                                         <select id="kelas" name="kelas" placeholder="Kelas" class="form-control">
                                            <option value="-">-</option>
                                            <option value="X-P1">X - P1</option>
                                            <option value="X-P2">X - P2</option>
                                            <option value="X-P3">X - P3</option>
                                            <option value="X-P4">X - P4</option>
                                            <option value="X-P5">X - P5</option>
                                            <option value="X-P6">X - P6</option>
                                            <option value="X-P7">X - P7</option>
                                            <option value="X-S1">X - S1</option>
                                            <option value="X-S2">X - S2</option>
                                            <option value="X-S3">X - S3</option>
                                            <option value="X-S4">X - S4</option>
                                            <option value="X-S5">X - S5</option>
                                            <option value="XI-P1">XI - P1</option>
                                            <option value="XI-P2">XI - P2</option>
                                            <option value="XI-P3">XI - P3</option>
                                            <option value="XI-P4">XI - P4</option>
                                            <option value="XI-P5">XI - P5</option>
                                            <option value="XI-P6">XI - P6</option>
                                            <option value="XI-P7">XI - P7</option>
                                            <option value="XI-S1">XI - S1</option>
                                            <option value="XI-S2">XI - S2</option>
                                            <option value="XI-S3">XI - S3</option>
                                            <option value="XI-S4">XI - S4</option>
                                            <option value="XI-S5">XI - S5</option>
                                         </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                         <select id="status" name="status" class="form-control">
                                             <option value="1">Aktif</option>
                                             <option value="2">Tidak aktif</option>
                                         </select>
                                    </div>
                                    
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" class="btn btn-primary btn-save btn-block" value="daftar" name="">
                                    <br>
                                    <a href="{{url('/home')}}" class="btn btn-primary btn-success btn-block">Kembali</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="{{asset('startmin-master/js/jquery.min.js')}}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('startmin-master/js/bootstrap.min.js')}}"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{asset('startmin-master/js/metisMenu.min.js')}}"></script>

        <!-- Custom Theme JavaScript -->
        <script src="{{asset('startmin-master/js/startmin.js')}}"></script>
        <!-- <script type="text/javascript">
            $('#peran').on('change',function(){
                console.log('0');
                var peran = $('#peran').val();
                if(peran=='01'){
                    $('#kelas').attr('disabled',true);
                }else{
                    $('#kelas').attr('disabled',false);
                }
            });
        </script> -->
    </body>
</html>
