@extends('layouts.app')

@section('content')
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
		<div class="col-md-12">
            <h1>Proposal Mengenai .....(judul_proposal)</h1>
            <form class="form-horizontal" action="" data-toggle="validator" method="post" id="form-learning-goal">
                {{ csrf_field()}} {{method_field('POST')}}
                <!-- <div class="modal-header"></div> -->
                <div class="modal-body">

                    <div class="col-md-5">
                    <a href="/babsatu" id="tambah" class="btn btn-warning btn-save btn-right">BAB 1</a>
                    <br>
                    <br>
                    <a href="#" id="tambah" class="btn btn-warning btn-save btn-right">BAB 2</a>
                    <br>
                    <br>
                    <a href="#" id="tambah" class="btn btn-warning btn-save btn-right">BAB 3</a>
                    </div>
                    <br>
                    <br>
                </div>
                <br>
                <br>
            </form>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@endsection