<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/ckeditor', function () {
//     return view('ck');
// });
// Route::post('tesckeditor/insert','tesckeditor@insert');
 //login




Auth::routes();
// Route::get('/logout', 'Auth\loginController@logout');
Route::get('/home', 'HomeController@index');
Route::get('/register', 'RegisterController@index');
Route::get('/', 'HomeAwalController@index');

//-------------------------------------MENU EKSTRAKURIKULER-------------------------------------
Route::get('/ekstrakurikuler/index/{id?}', 'EkstrakurikulerController@index');
Route::get('/ekstrakurikuler/create', 'EkstrakurikulerController@create');
Route::get('/ekstrakurikuler/edit/{id}', 'EkstrakurikulerController@edit');
Route::get('/ekstrakurikuler/delete/{id}', 'EkstrakurikulerController@destroy');
Route::post('/ekstrakurikuler/update/{id}', 'EkstrakurikulerController@update');
Route::post('/ekstrakurikuler/insert', 'EkstrakurikulerController@store');


//------------------------------------------------KONTEN DAN PENGUMUMAN KEGIATAN------------------------------------------------
Route::get('/tambah_foto_kegiatan', 'KontenController@tambahftkegiatansklh');
Route::get('/informasi_kegiatan', 'KontenController@infokegiatansklh');
Route::get('/tambah_acara_kegiatan', 'KontenController@tambahacrkegiatansklh');

//-----------------------------------------------------------PROPOSAL-------------------------------------------------------------
Route::get('/siswa/buatproposal', 'ProposalController@index');
Route::get('/guru/buatproposal', 'ProposalController@index');
Route::get('/tambahanggotaproposal/{id}', 'ProposalController@view_tambah_anggota');
Route::get('/hak_akses_proposal/{id}', 'ProposalController@hak_akses_proposal');



Route::get('/unggahfileproposal', 'HomeController@unggahfileprpslklsklh');

Route::post('/crud_tambah_anggota', 'ProposalController@crud_tambah_anggota');
Route::get('/get_anggota_proposal/{id}', 'ProposalController@get_anggota_proposal');

Route::post('/insert_tambah_anggota_halaman_proposal', 'ProposalController@insert_tambah_anggota_halaman_proposal');
Route::post('/update_isi_proposal/{id_halaman}', 'ProposalController@update_isi_proposal');
Route::get('/get_isi_halaman/{halaman_id}','ProposalController@get_isi_halaman');

Route::get('/formperbabproposal', 'HomeController@formperbabprpslkegiatansklh');

//---------------------------------------------------------MENU_USER-----------------------------------------------------------
Route::get('/dataanggotasekolah', 'MenuUserController@dataanggotasklh');








//-------------------------------------DATA DIRI-------------------------------------
Route::get('/data_diri', 'DataDiriController@datadirisklh');





Route::get('/ubahdatadirianggota', 'HomeController@ubahdatadirianggotasklh');

Route::post('/siswa/insertproposal', 'ProposalController@insertproposal');

Route::post('/siswa/view_insert_isi_proposal/{id}', 'ProposalController@view_insert_isi_proposal');

Route::get('/siswa/isi_proposal/{id}', 'ProposalController@view_insert_isi_proposal');

// Route::get('edit/proposal/{id}','ProposalController@')
//-------------------------------------LOGIN-------------------------------------
// Route::get('/login', function () {
//     return view('auth.login');
// });
// Route::get('/', function () {
//     return view('register');
// });

Route::post('/buatakun', 'RegisterController@buatakun');

// bagian admin



//-------------------------------------CKEDITOR-------------------------------------

//-------------------------------------BAB 1-------------------------------------
Route::get('/babsatu', 'CkEditorController@babsatuproposal');




// Route::get('/dataproposalkegiatan', 'HomeController@dataprpslkegiatansklh');
Route::get('/buatproposal', 'HomeController@buatprpslkegiatansklh');