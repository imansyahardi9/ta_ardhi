<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class ProposalController extends Controller
{
    public function index()
    {
    	$extrakulikuler = DB::table('ekstrakurikuler')->get();
    	$user =  DB::table('users')->where('peran',2)->get();
    	$proposal = DB::table('proposal')->join('proposal_has_user','proposal_has_user.proposal_idproposal','proposal.idproposal')->join('ekstrakurikuler','ekstrakurikuler.idekstrakurikuler','proposal.ekstrakurikuler_idekstrakurikuler')->where('proposal_has_user.users_idusers',Auth::user()->id)->get();
    	$proposal2 = array();
    	$i=0;
    	foreach ($proposal as $value) {
    		$proposal2[$i]['judulproposal'] = $value->judulproposal;
    		$proposal2[$i]['jenisproposal'] = $value->jenisproposal;
    		$proposal2[$i]['namaekskul'] = $value->namaekskul;
    		$proposal2[$i]['tahunajaranproprosal'] = $value->tahunajaranproprosal;
    		$proposal2[$i]['idproposal'] = $value->idproposal;
    		$proposal2[$i]['anggota'] = $this->get_anggota_nama_proposal($value->idproposal);
            $i++;
    	}
		return view('proposal.index',['extrakulikuler'=>$extrakulikuler,'proposal'=>$proposal2,'users'=>$user]);        
    }

    public function insertproposal(Request $request){
    	$judulproposal = $request['judulproposal'];
    	$jenisproposal = $request['jenisproposal'];
    	$tahunajaranproprosal = $request['tahunajaranproprosal'];
    	$ekstrakurikuler_idekstrakurikuler = $request['ekstrakurikuler_idekstrakurikuler'];
    	$id_proposal = DB::table('proposal')->insertGetId(['judulproposal'=>$judulproposal,'jenisproposal'=>$jenisproposal,'tahunajaranproprosal'=>$tahunajaranproprosal,'ekstrakurikuler_idekstrakurikuler'=>$ekstrakurikuler_idekstrakurikuler]);
    	DB::table('proposal_has_user')->insert(['proposal_idproposal'=>$id_proposal,'users_idusers'=>Auth::user()->id,'is_prposal_admin'=>1]);
    	return redirect('siswa/buatproposal');
    }

    public function view_insert_isi_proposal($id){
    	$halaman = DB::table('halaman')->where('proposal_id',$id)->get();
    	$text = "";
    	foreach ($halaman as $value) {
    		$text .= '<hr><h1><b>'.$value->keterangan.'</b> '.$this->get_nama_anggota_halaman($value->id).'</h1><div class="form-group"><br>'.
                    '<select class="select2" id="combobox-'.$value->id.'" onchange="set_header(\''.'halaman-'.$value->id.'\','.$value->id.')">
                        <option>Chose Header</option>
                    	<option value="Kata Pengantar">Kata Pengantar</option>
                    	<option value="Bab 1">Bab 1</option>
                    	<option value="Pendahuluan">Pendahuluan</option>
                    	<option value="A. Latar Belakang">A. Latar Belakang</option>
                    	<option value="B. Tujuan Kegiatan">B. Tujuan Kegiatan</option>
                    	<option value="Bab 2">Bab 2</option>
                    	<option value="Isi">Isi</option>
                    	<option value="C. Nama Kegiatan">C. Nama Kegiatan</option>
                    	<option value="D. Tema Kegiatan">D. Tema Kegiatan</option>
                    	<option value="E. Pelaksana Kegiatan">E. Pelaksana Kegiatan</option>
                    	<option value="F. Peserta">F. Peserta</option>
                    	<option value="G. Bahan Yang Dibutuhkan Saat Kegiatan">G. Bahan Yang Dibutuhkan Saat Kegiatan</option>
                    	<option value="H. Peraturan Peserta Kegiatan">H. Peraturan Peserta Kegiatan</option>
                    	<option value="Susunan Panitia">Susunan Panitia</option>
                    	<option value="I. Susunan Jadwal Kegiatan">I. Susunan Jadwal Kegiatan</option>
                    	<option value="J. Anggaran Dana">J. Anggaran Dana</option>
                    	<option value="Bab 3">Bab 3</option>
                    	<option value="Penutup">Penutup</option>
                    </select>
                    <div class="" style="float:right">
                    
                        Status Edit:
                        <select>
                            <option>tidak sedang edit</option>
                            <option>Ardi</option>
                            <option>Gagah</option>
                        </select>
                        <a href="" class="btn btn-primary">View</a>

                    </div>
                    <br>
                    <br>
                    <textarea name="halaman-'.$value->id.'" id="halaman-'.$value->id.'" cols="30" rows="10" class="form-control halaman"></textarea>'.
                '</div>';
    	}
    	
    	return view('proposal.isi_proposal',['proposal_id'=>$id,'halaman'=>$text,'halamans'=>$halaman]);
    }

    public function get_data_isi_proposal($id){
    	$halaman = DB::table('halaman')->where('proposal_id',$id)->get();
    	echo json_encode($halaman);
    }

    public function view_tambah_anggota($proposal_id){
    	$user =  DB::table('users')->where('peran',2)->get();
    	$user_belum_anggota = array();
    	$i = 0;
    	foreach ($user as $value) {
    		if($this->is_proposal_admin($value->id,$proposal_id)==false){
    			$user_belum_anggota[$i]['id'] = $value->id;
    			$user_belum_anggota[$i]['nama'] = $value->nama;
    			$i++;
    		}
    	}
    	return view('proposal.tambah_anggota',['users'=>$user_belum_anggota,'proposal_id'=>$proposal_id]);
    }

    public function crud_tambah_anggota(Request $request){
    	$anggota = $request['users_idusers'];
    	$proposal_idproposal = $request['proposal_idproposal'];
    	$is_null = DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_idproposal)->get();
    	if($is_null != null){
    		DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_idproposal)->delete();
    		DB::table('proposal_has_user')->insert(['proposal_idproposal'=>$proposal_idproposal,'users_idusers'=>Auth::user()->id,'is_prposal_admin'=>1]);
    	}
		foreach ($anggota as $value) {
			DB::table('proposal_has_user')->insert(['proposal_idproposal'=>$proposal_idproposal,'users_idusers'=>$value]);
		}

    	return redirect('siswa/buatproposal');
    }

    public function get_anggota_proposal($proposal_id){
    	$anggota = DB::table('proposal_has_user')->where('is_prposal_admin',0)->where('proposal_idproposal',$proposal_id)->get();
    	$data = array();
    	if($anggota!=null){
    		foreach ($anggota as $value) {
    			$data[] = $value->users_idusers;
	    	}
	    	echo json_encode($data);
    	}
    }

    public function get_anggota_nama_proposal($proposal_id){
    	$anggota = DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_id)->join('users','users.id','proposal_has_user.users_idusers')->get();
    	$text = "";
		foreach ($anggota as $value) {
			$text .= '<span style="background:#ffbc00;" class="label label-warning">'.$value->nama.'</span> ';
    	}
    	return $text;
    	

    }

    public function hak_akses_proposal($proposal_id){
    	$anggota = DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_id)->get();
    	$halaman = DB::table('halaman')->where('proposal_id',$proposal_id)->orderBy('id')->get();
    	$user =  DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_id)->join('users','users.id','proposal_has_user.users_idusers')->get();
    	$halaman2 = array();
    	$i=0;
    	foreach ($halaman as $value) {
    		$halaman2[$i]['keterangan'] = $value->keterangan;
    		$halaman2[$i]['isi'] = $value->isi;
    		$halaman2[$i]['proposal_id'] = $value->proposal_id;
    		$halaman2[$i]['id'] = $value->id;
    		$halaman2[$i]['anggota'] = $this->get_nama_anggota_halaman($value->id);
    		$i++;
    	}
    	
    	return view('proposal.hak_akses_proposal',['anggota'=>$anggota,'halaman'=>$halaman2,'proposal_id'=>$proposal_id,'users'=>$user]);
        
    }

    public function get_nama_anggota_halaman($halaman_id){
    	$anggota = DB::table('user_has_halaman')->where('halaman_id',$halaman_id)->join('users','users.id','user_has_halaman.user_id')->get();
    	$text = "";
    	foreach ($anggota as $value) {
    		$text .= '<span style="background:#ffbc00;" class="label label-warning">'.$value->nama.'</span> ';
    	}
    	return $text;
    }

    public function is_proposal_admin($user_id,$proposal_id){
    	$anggota = DB::table('proposal_has_user')->where('is_prposal_admin',1)->where('users_idusers',$user_id)->where('proposal_idproposal',$proposal_id)->first();
    	if($anggota != null){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function insert_tambah_anggota_halaman_proposal(Request $request){
    	$anggota = $request['user_id'];
    	$keterangan = $request['keterangan'];
    	$proposal_id = $request['proposal_id'];
    	$halaman_id = DB::table('halaman')->insertGetId(['keterangan'=>$keterangan,'proposal_id'=>$proposal_id]);

		foreach ($anggota as $value) {
			DB::table('user_has_halaman')->insert(['user_id'=>$value,'halaman_id'=>$halaman_id]);
		}
    	return redirect('hak_akses_proposal/'.$proposal_id);
    }
    
    public function update_tambah_anggota_halaman_proposal(Request $request){
    	$anggota = $request['users_idusers'];
    	$proposal_idproposal = $request['proposal_idproposal'];
    	$is_null = DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_idproposal)->get();
    	if($is_null != null){
    		DB::table('proposal_has_user')->where('proposal_idproposal',$proposal_idproposal)->delete();
    		DB::table('proposal_has_user')->insert(['proposal_idproposal'=>$proposal_idproposal,'users_idusers'=>Auth::user()->id,'is_prposal_admin'=>1]);
    	}
		foreach ($anggota as $value) {
			DB::table('proposal_has_user')->insert(['proposal_idproposal'=>$proposal_idproposal,'users_idusers'=>$value]);
		}
    	return redirect('siswa/buatproposal');
    }

    public function update_isi_proposal(Request $request,$halaman_id){
    	$isi = $request['isi_proposal'];
    	DB::table('halaman')->where('id',$halaman_id)->update(['isi'=>$isi]);
    }

    public function get_isi_halaman($halaman_id){
    	$halaman = DB::table('halaman')->where('id',$halaman_id)->first();
    	echo $halaman->isi;
    }



}
