<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class EkstrakurikulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        if($id==null){
            $ekstrakurikulers = DB::table('ekstrakurikuler')->orderBy('idekstrakurikuler')->first();
            $id= $ekstrakurikulers->idekstrakurikuler;
        }
        $ekstrakurikulers = DB::table('ekstrakurikuler')->orderBy('idekstrakurikuler','desc')->get();
        $ekstrakurikuler = DB::table('ekstrakurikuler')->where('idekstrakurikuler',$id)->first();
        return view('ekstrakurikuler.index',['ekstrakurikuler'=>$ekstrakurikuler,'ekstrakurikulers'=>$ekstrakurikulers,'id'=>$id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(date('n') >6){
            $tahunajaran = date('Y').'/'.(date('Y')+1);
        }else{
            $tahunajaran = (date('Y')-1).'/'.date('Y');
        }
        return view('ekstrakurikuler.create',['tahun_ajaran'=>$tahunajaran]);
    }

    public function menuEkskul()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate(
        [
            'nama_ekskul' => 'required|string|max:255|unique:ekstrakurikuler,namaekskul',
            'deskripsi_kegiatan' => 'required|string',
            'tahun_ajaran' => 'required',
            'logo_ekskul' => 'required',
        ]);
        $file = $request->file('logo_ekskul');
        $file_name = rand().'.'.$file->getClientOriginalExtension();
        $lokasi = 'logo_ekskul/'.$file_name;
        $tujuan_upload = 'logo_ekskul';
        $file->move($tujuan_upload,$file_name);
        $id = DB::table('ekstrakurikuler')->insertGetId(['namaekskul'=>$request['nama_ekskul'],'deskripsiekskul'=>$request['deskripsi_kegiatan'],'tahunajaran'=>$request['tahun_ajaran'],'logo_ekskul'=>$lokasi]);
        return redirect('ekstrakurikuler/index/'.$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(date('n') >6){
            $tahunajaran = date('Y').'/'.(date('Y')+1);
        }else{
            $tahunajaran = (date('Y')-1).'/'.date('Y');
        }
        $ekstrakurikuler = DB::table('ekstrakurikuler')->where('idekstrakurikuler',$id)->first();
        return view('ekstrakurikuler.edit',['ekstrakurikuler'=>$ekstrakurikuler,'tahun_ajaran'=>$tahunajaran]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate(
        [
            'nama_ekskul' => 'required|string|max:255|unique:ekstrakurikuler,namaekskul',
            'deskripsi_kegiatan' => 'required|string',
            'tahun_ajaran' => 'required',
        ]);
        $file = $request->file('logo_ekskul');
        if(!$file){
            DB::table('ekstrakurikuler')->where('idekstrakurikuler',$id)->update(['namaekskul'=>$request['nama_ekskul'],'deskripsiekskul'=>$request['deskripsi_kegiatan'],'tahunajaran'=>$request['tahun_ajaran']]);
            return redirect('ekstrakurikuler/index/'.$id);
        }
        $file_name = rand().'.'.$file->getClientOriginalExtension();
        $lokasi = 'logo_ekskul/'.$file_name;
        $tujuan_upload = 'logo_ekskul';
        $file->move($tujuan_upload,$file_name);
        DB::table('ekstrakurikuler')->where('idekstrakurikuler',$id)->update(['namaekskul'=>$request['nama_ekskul'],'deskripsiekskul'=>$request['deskripsi_kegiatan'],'tahunajaran'=>$request['tahun_ajaran'],'logo_ekskul'=>$lokasi]);
        return redirect('ekstrakurikuler/index/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('ekstrakurikuler')->where('idekstrakurikuler',$id)->delete();
        return redirect('ekstrakurikuler/index');
    }

    public function tambahanggota_index(Type $var = null)
    {
        # code...
    }


}
